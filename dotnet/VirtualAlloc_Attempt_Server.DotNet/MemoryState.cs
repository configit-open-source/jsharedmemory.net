﻿namespace Server.DotNet {
  public enum MemoryState {
    Idle = 0,
    RequestLocked = 1,
    RequestReady = 2,
    ResponseLocked = 3,
    ResponseReady = 4
  }
}