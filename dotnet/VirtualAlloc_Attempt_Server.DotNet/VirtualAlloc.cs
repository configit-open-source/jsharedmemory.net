﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Server.DotNet {
  public class SharedMemory {

    private readonly UIntPtr _memStart;

    public SharedMemory( uint size ) {
      _memStart = VirtualAlloc( new UIntPtr(0), new UIntPtr( size ), AllocationType.PHYSICAL, MemoryProtection.READWRITE );
    }

    public SharedMemory( UIntPtr otherProcessMemoryPtr ) {
      _memStart = otherProcessMemoryPtr;
    }

    public void Transition( MemoryState oldState, MemoryState newState ) {
      while ( true ) {
        var exchange = (int) newState;

        if ( exchange == InterlockedCompareExchange( _memStart, exchange, (int) oldState ) ) {
          break;
        }
        Thread.Yield();
      }
    }

    [DllImport( "kernel32.dll" )]
    public static extern int InterlockedCompareExchange( UIntPtr destination, int exchange,int comperand );

    [DllImport( "kernel32.dll", SetLastError = true )]
    public static extern UIntPtr VirtualAlloc(
          UIntPtr lpAddress,
          UIntPtr dwSize,
          AllocationType flAllocationType,
          MemoryProtection flProtect );

    [DllImport( "kernel32.dll", SetLastError = true )]
    public static extern bool VirtualFree(
          UIntPtr lpAddress,
          UIntPtr dwSize,
          uint dwFreeType );

    [DllImport( "kernel32.dll", SetLastError = true )]
    public static extern bool WriteProcessMemory(
    IntPtr hProcess,
    IntPtr lpBaseAddress,
    byte[] lpBuffer,
    int nSize,
    out IntPtr lpNumberOfBytesWritten );

    [DllImport( "kernel32.dll", SetLastError = true )]
    public static extern bool WriteProcessMemory(
        IntPtr hProcess,
        IntPtr lpBaseAddress,
        IntPtr lpBuffer,
        int nSize,
        out IntPtr lpNumberOfBytesWritten );


  }
}
