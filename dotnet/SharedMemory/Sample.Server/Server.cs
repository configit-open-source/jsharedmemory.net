﻿using System;
using System.IO;
using System.Text;

using SharedMemory;

namespace Sample {
  public class Server: SharedMemoryServer {
    public static void Main( string[] args ) {
      new Server( args[0], int.Parse( args[1] ) ).Listen();
    }

    public Server( string mapName, int size )
      : base( mapName, size ) {
    }

    protected override void ProcessRequest( Stream stream ) {
      int request;
      using ( var s = new BinaryReader( stream, Encoding.UTF8, true ) ) {
        request = s.ReadInt32();
        Console.WriteLine( "Server got request: " + request );
      }

      stream.Seek( 0, SeekOrigin.Begin );

      using ( var s = new BinaryWriter( stream, Encoding.UTF8, true ) ) {
        s.Write( request + 42 );
        Console.WriteLine( "Wrote response: " + ( request + 42 ) );
      }
    }
  }
}