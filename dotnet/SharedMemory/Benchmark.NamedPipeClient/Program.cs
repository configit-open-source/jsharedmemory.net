﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Pipes;
using System.Security.Principal;
using System.Text;
using System.Threading;

namespace Benchmark.NamedPipeClient {
  class Program {
    static void Main( string[] args ) {
      int msgSize = int.Parse( args[0] );

      var serverIn = new NamedPipeClientStream( ".", "server-in", PipeDirection.Out, PipeOptions.WriteThrough, TokenImpersonationLevel.Impersonation );
      var serverOut = new NamedPipeClientStream( ".", "server-out", PipeDirection.In, PipeOptions.WriteThrough, TokenImpersonationLevel.Impersonation );
      serverIn.Connect();
      serverOut.Connect();

      Stopwatch timer = Stopwatch.StartNew();

      var iterations = 250000;
      byte[] buffer = new byte[msgSize];

      for ( int i = 1; i < iterations; i++ ) {

        serverIn.Write( buffer, 0, buffer.Length );

        Thread.Yield();

        serverOut.Read( buffer, 0, buffer.Length );
      }

      serverIn.Dispose();
      serverOut.Dispose();
      Console.WriteLine(
        $"Finished in {timer.ElapsedMilliseconds} ms with avg of {timer.ElapsedMilliseconds * 1000 / (double) iterations} microseconds" );
    }
  }
}
