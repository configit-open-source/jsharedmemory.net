﻿using System;
using System.IO.Pipes;
using System.Threading;
using System.Threading.Tasks;

namespace Benchmark.NamedPipeServer {
  class Server {
    static void Main( string[] args ) {
      int msgSize = int.Parse( args[0] );

      var pipeServerIn = new NamedPipeServerStream( "server-in", PipeDirection.In, 1, PipeTransmissionMode.Byte, PipeOptions.WriteThrough );
      var pipeServerOut = new NamedPipeServerStream( "server-out", PipeDirection.Out, 1, PipeTransmissionMode.Byte, PipeOptions.WriteThrough );

      var inTask = pipeServerIn.WaitForConnectionAsync();
      var outTask = pipeServerOut.WaitForConnectionAsync();

      Task.WaitAll( inTask, outTask );
      Console.WriteLine( "Client connected!" );

      byte[] buffer = new byte[msgSize];

      while ( true ) {
        //if ( pipeServerIn.Position == pipeServerIn.Length ) {
        //  continue;
        //}

        pipeServerIn.Read( buffer, 0, buffer.Length );
        Thread.Yield();

        pipeServerOut.Write( buffer, 0, buffer.Length );
      }
    }
  }
}