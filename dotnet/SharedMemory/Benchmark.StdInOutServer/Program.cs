﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace Benchmark.StdInOutServer {
  class Program {
    static void Main( string[] args ) {
      int bufferSize = int.Parse( args[0] );
      int msgSize = int.Parse( args[1] );
      int pid = int.Parse( args[2] );

      RunAsServer( bufferSize, msgSize, pid );
    }

    private static void RunAsServer( int bufferSize, int msgSize, int parentPid ) {
      var input = Console.OpenStandardInput( bufferSize );
      var output = Console.OpenStandardOutput( bufferSize );
      var processById = Process.GetProcessById( parentPid );

      byte[] buffer = new byte[msgSize];

      while ( true ) {

        if ( ( DateTime.Now.Second % 5 ) == 0 && processById.HasExited ) {
          return; // self-destruct if parent is gone
        }

        Thread.Yield();
        input.Read( buffer, 0, buffer.Length );
        output.Write( buffer, 0, buffer.Length );
        output.Flush();
      }
    }
  }
}