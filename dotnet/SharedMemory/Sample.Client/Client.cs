﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;

using SharedMemory;

namespace Sample {
  public class Client: SharedMemoryClient<int, int> {

    private readonly BinaryReader _reader;
    private readonly BinaryWriter _writer;

    public static void Main( string[] args ) {

      string mapName = args[0];
      int size = int.Parse( args[1] );

      var client = new Client( mapName, size );
      for ( int i = 1; i < 100; i++ ) {
        Console.WriteLine( "Hit key to send " + i );
        Console.ReadKey();

        Stopwatch timer = new Stopwatch();
        var latency = client.Process( i );
        Console.WriteLine( "Response in : " + timer.Elapsed.TotalMilliseconds );

        Console.WriteLine( $"Process({i}) = {latency}" );
      }
    }

    public Client( string mapName, int size ) : base( mapName, size ) {
      _writer = new BinaryWriter( Buffer, Encoding.UTF8, true );
      _reader = new BinaryReader( Buffer, Encoding.UTF8, true );
    }

    public override void WriteRequest( int request ) {
      Console.WriteLine( "Writing request: " + request );
      _writer.Write( request );
      _writer.Flush();

    }

    public override int ReadResponse() {
      return _reader.ReadInt32();
    }
  }
}