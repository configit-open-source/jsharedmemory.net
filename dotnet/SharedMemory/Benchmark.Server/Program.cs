﻿using System.IO;

using SharedMemory;

namespace Benchmark.Server {
  public class Server: SharedMemoryServer {
    private readonly int _msgSize;

    private readonly byte[] _buffer;

    public static void Main( string[] args ) {
      var msgSize = int.Parse( args[0] );
      new Server( "Benchmark", 2 * msgSize, msgSize ).Listen();
    }

    public Server( string mapName, int size, int msgSize )
      : base( mapName, size ) {
      _msgSize = msgSize;
      _buffer = new byte[_msgSize];
    }

    protected override void ProcessRequest( Stream stream ) {
      stream.Read( _buffer, 0, _msgSize );

      stream.Seek( 0, SeekOrigin.Begin );

      stream.Write( _buffer, 0, _msgSize );
      stream.Flush();
    }
  }
}