﻿using System;
using System.IO;
using System.Threading;

namespace SharedMemory {
  public abstract class SharedMemoryServer: IDisposable {

    private readonly SharedMemory _memory;

    protected SharedMemoryServer( string mapName, int size ) {
      _memory = SharedMemory.CreateOrOpen( mapName, size );
    }

    /// <summary>
    /// Start the server, optionally providing a <see cref="CancellationToken"/>
    /// </summary>
    public void Listen( CancellationToken cancellationToken = default( CancellationToken ) ) {
      while ( !cancellationToken.IsCancellationRequested ) {
        _memory.Transition( MemoryState.RequestReady, MemoryState.ResponseInProgress );

        _memory.Buffer.Position = 0;
        ProcessRequest( _memory.Buffer );

        _memory.Transition( MemoryState.ResponseInProgress, MemoryState.ResponseReady );

        Thread.Yield();
      }
    }

    /// <summary>
    /// Called when a request is ready (and locked) in the buffer.
    /// The implementation should read the request, and replace 
    /// it with the response.
    /// </summary>
    protected abstract void ProcessRequest( Stream stream );

    public void Dispose() {
      // TODO: Proper dispose pattern
      _memory.Dispose();
    }
  }
}