﻿namespace SharedMemory {
  public enum MemoryState {
    Idle = 0,
    ResponseInProgress = 1,
    RequestInProgress = 2,
    RequestReady = 3,
    ResponseReady = 4
  }
}