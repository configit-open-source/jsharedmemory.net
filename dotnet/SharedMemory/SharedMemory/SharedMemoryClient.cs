﻿using System;
using System.IO;

namespace SharedMemory {
  public abstract class SharedMemoryClient<TRequest,TResponse> : IDisposable {
    private readonly SharedMemory _memory;

    protected SharedMemoryClient( string mapName, int size ) {
      _memory = SharedMemory.CreateOrOpen( mapName, size );
    }

    /// <summary>
    /// Process a request.
    /// </summary>
    public TResponse Process( TRequest request ) {
      _memory.Transition( MemoryState.Idle, MemoryState.RequestInProgress );

      _memory.Buffer.Position = 0;

      WriteRequest( request );

      _memory.Transition( MemoryState.RequestInProgress, MemoryState.RequestReady );

      // The server will process the request, and set MemoryState.ResponseReady
      _memory.Transition( MemoryState.ResponseReady, MemoryState.Idle );

      _memory.Buffer.Position = 0;

      return ReadResponse();
    }

    public Stream Buffer => _memory.Buffer;

    /// <summary>
    /// Write request to <see cref="Stream"/> at current position.
    /// </summary>
    public abstract void WriteRequest( TRequest request );

    /// <summary>
    /// Read response from <see cref="Stream"/> from current position.
    /// </summary>
    public abstract TResponse ReadResponse();

    public void Dispose() {
      // TODO: Proper dispose pattern
      _memory.Dispose();
    }
  }
}