﻿using System;
using System.Diagnostics;
using System.IO.MemoryMappedFiles;
using System.Threading;

namespace SharedMemory {
  public class SharedMemory: IDisposable {
    private readonly MemoryMappedFile _mapped;

    private readonly IntPtr _lockPtr;

    /// <summary>
    /// Stores a view on a single 32-bit integer that is used to indicate the
    /// lock state. <see cref="MemoryState"/>
    /// </summary>
    private readonly MemoryMappedViewAccessor _lockView;

    /// <summary>
    /// The part of the shared memory that is available for request and response objects.
    /// </summary>
    public MemoryMappedViewStream Buffer { get; }

    public static SharedMemory CreateNew( string mapName, int sizeInBytes ) {
      return new SharedMemory( MemoryMappedFile.CreateNew( mapName, sizeInBytes ), sizeInBytes );
    }

    public static SharedMemory OpenExisting( string mapName, int sizeInBytes ) {
      return new SharedMemory( MemoryMappedFile.OpenExisting( mapName, MemoryMappedFileRights.ReadWrite ), sizeInBytes );
    }

    public static SharedMemory CreateOrOpen( string mapName, int sizeInBytes ) {
      return new SharedMemory( MemoryMappedFile.CreateOrOpen( mapName, sizeInBytes, MemoryMappedFileAccess.ReadWrite ), sizeInBytes );
    }

    private SharedMemory( MemoryMappedFile mapped, int size ) {
      _mapped = mapped;
      _lockView = mapped.CreateViewAccessor( 0, sizeof( int ) );

      // TODO: This is dangerous if we didnt create it!?
      _lockView.Write( 0, (int) MemoryState.Idle ); // Make sure its idle initially
      Buffer = mapped.CreateViewStream( sizeof( int ), size - sizeof( int ), MemoryMappedFileAccess.ReadWrite );
      Debug.Assert( Buffer.CanSeek );
      _lockPtr = _lockView.SafeMemoryMappedViewHandle.DangerousGetHandle();
    }

    public unsafe void Transition( MemoryState oldState, MemoryState newState ) {
      while ( true ) {
        var exchange = (int) newState;
        var comparand = (int) oldState;

        var actualOldState = Interlocked.CompareExchange( ref *(int*) _lockPtr, exchange, comparand );

        if ( comparand == actualOldState ) {
          break;
        }
        Thread.Yield(); // to reduce overhead of busy polling
      }
    }

    public void Dispose() {
      _lockView.Dispose();
      Buffer.Dispose();
      _mapped.Dispose();
    }
  }
}