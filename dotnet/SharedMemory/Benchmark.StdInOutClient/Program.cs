﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;

namespace Benchmark.StdInOut {
  class Program {
    public static void Main( string[] args ) {
      RunAsClient( int.Parse( args[0] ), int.Parse( args[1] ) );
    }

    private static void RunAsClient( int streamBufferSize, int msgSize ) {
      var fileName = Path.Combine( Assembly.GetExecutingAssembly().Location.Replace( 
        "Benchmark.StdInOutClient", "Benchmark.StdInOutServer" ) );

      var startInfo =
        new ProcessStartInfo( fileName, $"{streamBufferSize} {msgSize} {Process.GetCurrentProcess().Id}" ) {
          UseShellExecute = false,
          CreateNoWindow = true,
          RedirectStandardInput = true,
          RedirectStandardOutput = true
        };

      Console.WriteLine( "Running: " + startInfo.FileName );

      var server = Process.Start( startInfo );

      var clientOutput = server.StandardOutput.BaseStream;
      var clientInput = server.StandardInput.BaseStream;

      Stopwatch timer = Stopwatch.StartNew();

      const int iterations = 250000;
      int i = -1;
      byte[] buffer = new byte[msgSize];

      try {

        for ( i = 0; i < iterations; i++ ) {
          clientInput.Write( buffer, 0, buffer.Length );
          clientInput.Flush();
          Thread.Yield();
          clientOutput.Read( buffer, 0, buffer.Length );

        }
      }
      catch ( IOException e ) { Console.WriteLine( "Exception at i = " + i + "\n" + e ); }

      Console.WriteLine(
  $"Finished in {timer.ElapsedMilliseconds} ms with avg of {timer.ElapsedMilliseconds * 1000 / (double) iterations} microseconds" );
    }
  }
}