﻿using System;
using System.Diagnostics;

using SharedMemory;

namespace Benchmark.Client {
  public class Client: SharedMemoryClient<int, int> {
    private readonly int _messageSize;

    public static void Main( string[] args ) {
      var messageSize = int.Parse( args[0] );
      
      var client = new Client( "Benchmark", 2*messageSize, messageSize );
      Stopwatch timer = Stopwatch.StartNew();

      var iterations = 250000;
      Console.WriteLine( $"Running {iterations} of {messageSize} bytes each" );

      for ( int i = 1; i < iterations; i++ ) {
        client.Process( i );
      }
      Console.WriteLine(
        $"Finished in {timer.ElapsedMilliseconds} ms with avg of {timer.ElapsedMilliseconds * 1000 / (double) iterations} microseconds" );
    }

    public Client( string mapName, int size, int messageSize )
      : base( mapName, size ) {
      _messageSize = messageSize;
      _buffer = new byte[messageSize];
    }

    public override void WriteRequest( int request ) {
      Buffer.Write( _buffer, 0, _messageSize );
      Buffer.Flush();
    }

    private readonly byte[] _buffer;

    public override int ReadResponse() {
      Buffer.Read( _buffer, 0, _messageSize );
      return 0;
    }
  }
}