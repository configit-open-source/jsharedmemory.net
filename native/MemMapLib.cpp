// dll implementation file MemMapLib.cpp
//
////////////////////////////////////////////////////////////////////////////////////

#include <windows.h>
#include <tchar.h>

#include "MemMapFile.h"

#define UWM_DATA_READY_MSG _T("UWM_DATA_READY_MSG-{7FDB2CB4-5510-4d30-99A9-CD7752E0D680}")

UINT UWM_DATA_READY;

BOOL APIENTRY DllMain(HINSTANCE hinstDll, DWORD dwReasion, LPVOID lpReserved) {

	if(dwReasion == DLL_PROCESS_ATTACH) 
		UWM_DATA_READY = RegisterWindowMessage(UWM_DATA_READY_MSG);

	return TRUE; 
}

jint throwException(JNIEnv *env, char *message) {
  jclass exClass;
  char *className = "java/lang/RuntimeException";

  exClass = env->FindClass(className);
  if (exClass == nullptr) {
    throw "Should not happen!";
  }

  return env->ThrowNew(exClass, message);
}

/*
hMapFile = CreateFileMapping(
INVALID_HANDLE_VALUE,    // use paging file
NULL,                    // default security
PAGE_READWRITE,          // read/write access
0,                       // maximum object size (high-order DWORD)
BUF_SIZE,                // maximum object size (low-order DWORD)
szName);                 // name of mapping object

*/
JNIEXPORT jlong JNICALL Java_com_memorymapping_MemMapFile_createFileMapping
	(JNIEnv * pEnv, jclass, jint lProtect, jint dwMaximumSizeHigh, 
	jint dwMaximumSizeLow, jstring name) {
	
	HANDLE hMapFile = nullptr;

	LPCSTR lpName = pEnv->GetStringUTFChars(name, nullptr);

	__try {
		hMapFile = CreateFileMapping(INVALID_HANDLE_VALUE, nullptr, lProtect, dwMaximumSizeHigh, 									 dwMaximumSizeLow, lpName);

		if(hMapFile == nullptr) {
      throwException(pEnv, "Can not create file mapping object");
			__leave;
		}

		if(GetLastError() == ERROR_ALREADY_EXISTS) {
      throwException(pEnv, "File mapping object already exists");
			CloseHandle(hMapFile);		
			__leave;
		}
	}
	__finally {
	}

	pEnv->ReleaseStringUTFChars(name, lpName);

	// if hMapFile is NULL, just return NULL, or return the handle
	return reinterpret_cast<jlong>(hMapFile);
}

JNIEXPORT jlong JNICALL Java_com_memorymapping_MemMapFile_openFileMapping
	(JNIEnv * pEnv, jclass, jint dwDesiredAccess,	jboolean bInheritHandle, jstring name) {
	
	HANDLE hMapFile;

	LPCSTR lpName = pEnv->GetStringUTFChars(name, nullptr);
	hMapFile = OpenFileMapping(dwDesiredAccess, bInheritHandle, lpName);
  if (hMapFile == nullptr) throwException(pEnv, "Can not open file mapping object");
	pEnv->ReleaseStringUTFChars(name, lpName);

	return reinterpret_cast<jlong>(hMapFile);
}

JNIEXPORT jlong JNICALL Java_com_memorymapping_MemMapFile_mapViewOfFile
	(JNIEnv* pEnv, jclass, jlong hMapFile, jint dwDesiredAccess, 
	jint dwFileOffsetHigh, jint dwFileOffsetLow, jint dwNumberOfBytesToMap) {
	
	PVOID pView;
	pView = MapViewOfFile(reinterpret_cast<HANDLE>(hMapFile), dwDesiredAccess, 
						  dwFileOffsetHigh, dwFileOffsetLow, dwNumberOfBytesToMap);
	if(pView == nullptr) throwException(pEnv, "Can not map view of file");

	return reinterpret_cast<jlong>(pView);
}

JNIEXPORT jboolean JNICALL Java_com_memorymapping_MemMapFile_unmapViewOfFile
	(JNIEnv* pEnv, jclass, jlong lpBaseAddress) {
  auto bRet = UnmapViewOfFile(reinterpret_cast<PVOID>(lpBaseAddress));
	if(!bRet) throwException(pEnv, "Can not unmap view of file");

	return bRet;
}

JNIEXPORT jboolean JNICALL Java_com_memorymapping_MemMapFile_closeHandle
	(JNIEnv *, jclass, jlong hObject) {
  
	return CloseHandle(reinterpret_cast<HANDLE>(hObject));
}