package com.memorymapping;

import java.io.IOException;
import java.util.UUID;

import com.sun.jna.win32.*;

public class Main {

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException, NoSuchMethodException, ClassNotFoundException, IOException {
        UUID id = java.util.UUID.randomUUID();
        String mapName = "jsharedmemory.net-" + id.toString();

        new SharedMemory("test", 100);
    }
}
