package com.memorymapping;

import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.WinBase;
import com.sun.jna.platform.win32.WinNT;
import sun.misc.Unsafe;
import sun.nio.ch.FileChannelImpl;


import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.reflect.*;
import java.io.FileDescriptor;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

import static com.sun.jna.platform.win32.WinNT.PAGE_READWRITE;

/**
 * Created by pt on 07-Aug-16.
 */
public class SharedMemory {
//
//    public static SharedMemory CreateNew( string mapName, int sizeInBytes ) {
//        return new SharedMemory( MemoryMappedFile.CreateNew( mapName, sizeInBytes ), sizeInBytes );
//    }
//
//    public static SharedMemory OpenExisting( string mapName, int sizeInBytes ) {
//        return new SharedMemory( MemoryMappedFile.OpenExisting( mapName, MemoryMappedFileRights.ReadWrite ), sizeInBytes );
//    }
//
//    public static SharedMemory CreateOrOpen( string mapName, int sizeInBytes ) {
//        return new SharedMemory( MemoryMappedFile.CreateOrOpen( mapName, sizeInBytes, MemoryMappedFileAccess.ReadWrite ), sizeInBytes );
//    }

    public SharedMemory(String name, int size)
            throws NoSuchFieldException,
                   IllegalAccessException,
                   ClassNotFoundException,
                   NoSuchMethodException,
                   IOException {
        WinBase.SECURITY_ATTRIBUTES attributes = new WinBase.SECURITY_ATTRIBUTES();
        attributes.bInheritHandle = true;

        WinNT.HANDLE handle = Kernel32.INSTANCE.CreateFileMapping(
                new WinNT.HANDLE(Pointer.NULL), attributes, PAGE_READWRITE, 0, size, name);

        Field f = Unsafe.class.getDeclaredField("theUnsafe");
        f.setAccessible(true);
        Unsafe unsafe = (Unsafe) f.get(null);

        WinNT.

    }

    private static long GetUnderlyingPointer(Pointer pointer) throws NoSuchFieldException, IllegalAccessException {
        Field peerField = Pointer.class.getDeclaredField("peer");

        peerField.setAccessible(true);
        return peerField.getLong(pointer);
    }

    private static FileDescriptor CreateFD(WinNT.HANDLE handle) throws NoSuchFieldException, IllegalAccessException {
        FileDescriptor descriptor = new FileDescriptor();

        Field field = FileDescriptor.class.getDeclaredField("handle");
        field.setAccessible(true);
        field.setLong(descriptor, GetUnderlyingPointer(handle.getPointer()));

        assert (descriptor.valid());
        return descriptor;
    }
}
